%Includes Residues for Contact energies

% Function to read a .coor file and output the x,y,z coordinates of all
% alpha carbons for syntaxin and SNAP25#1 & #2
% 1 = VAMP (Synaptobrevin)
% 2 = Syntaxin
% 3 = SNAP-25 #1
% 4 = SNAP-25 #2
% 5 = Complexin

function [x,y,z] = txt_reader(fn)

fid=fopen(fn); % Open specified .coor file

C = textscan(fid, '%s %u %s %s %s %u %f %f %f %f %f %s','commentStyle', 'REMARK');

%Assemble Data into variables
atom=C{1};
atomno=C{2};
atomspec=C{3};
col=C{4};
letter=C{5};
residno=C{6};
x=C{7};
y=C{8};
z=C{9};
col1=C{10};
col2=C{11};
col3=C{12};

x=x*10^-10;
y=y*10^-10;
z=z*10^-10;


fclose('all');
end


