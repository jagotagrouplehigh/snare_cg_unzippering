function [ffbx, ffby, ffbz]=Force_FENE_Bond(x,y,z,fbspring,bnum_total)

%#codegen
coder.inline('never')

ffbx = zeros(bnum_total,1);
ffby = zeros(bnum_total,1);
ffbz = zeros(bnum_total,1);


% Determine number of springs (i.e. number of rows in spring array)

c = length(fbspring(:,1));
l = zeros(c,1);
rinf = fbspring(:,5);
delta = fbspring(:,3);
r_c = 0.9*rinf;


for i = 1:c
    
    %Calculate distance between 2 beads connected by a spring
    l(i,1) = sqrt((x(fbspring(i,1))-x(fbspring(i,2)))^2+(y(fbspring(i,1))-y(fbspring(i,2)))^2+(z(fbspring(i,1))-z(fbspring(i,2)))^2);
    ks=fbspring(i,4);
    
    %Calculate force due to spring (F = -kx)
    
    if l<r_c(c)
        ffbxtemp = -ks*(l(i,1)-delta(i))*(x(fbspring(i,1))-x(fbspring(i,2)))/(l(i,1)*(1-((l(i,1)-delta(i))/rinf(i))^2));
        ffbytemp = -ks*(l(i,1)-delta(i))*(y(fbspring(i,1))-y(fbspring(i,2)))/(l(i,1)*(1-((l(i,1)-delta(i))/rinf(i))^2));
        ffbztemp = -ks*(l(i,1)-delta(i))*(z(fbspring(i,1))-z(fbspring(i,2)))/(l(i,1)*(1-((l(i,1)-delta(i))/rinf(i))^2));
        
    else
        ffbxtemp = -ks*(x(fbspring(i,1))-x(fbspring(i,2)))*(1-delta(i)/l(i,1));
        ffbytemp = -ks*(y(fbspring(i,1))-y(fbspring(i,2)))*(1-delta(i)/l(i,1));
        ffbztemp = -ks*(z(fbspring(i,1))-z(fbspring(i,2)))*(1-delta(i)/l(i,1));
    end
    
    %Calculate forces on each bead in opposite directions. Add this to the
    %existing forces due to other springs on the given beads
    ffbx(fbspring(i,1),1) = ffbx(fbspring(i,1),1) + ffbxtemp;
    ffby(fbspring(i,1),1) = ffby(fbspring(i,1),1) + ffbytemp;
    ffbz(fbspring(i,1),1) = ffbz(fbspring(i,1),1) + ffbztemp;
    
    ffbx(fbspring(i,2),1) = ffbx(fbspring(i,2),1) - ffbxtemp;
    ffby(fbspring(i,2),1) = ffby(fbspring(i,2),1) - ffbytemp;
    ffbz(fbspring(i,2),1) = ffbz(fbspring(i,2),1) - ffbztemp;
    
end


