% This code starts with a minimized SNARE bundle structure. A bead attached 
% to the C-terminal end of Syx is held fixed. Force control is applied to a 
% pulling bead attached to the C-terminal end of Syb. The pulling bead is
% pulled along a vector that unzippers the SNARE bundle. During
% unzippering, Syb is unraveled/raveled in stages dictated by a force
% dependent Monte Carlo algorithm.

clear all;
clc;
close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%   PARAMETERS and FOLDERS %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lambdamult = 2.3; %Miyazawa and Jernigan Parameter Multiplier
Rcmult = 0.8; %Miyazawa and Jernigan Parameter Multiplier
e0mult = -0.42; %Miyazawa and Jernigan Parameter Multiplier
sigma=0.4;% for Miyazawa Jernighan potential
dtmult=1; % Change time step if needed
folder=0.0963;% Related to how folders are names
ksnew=0.168525;% spring constant for ENM
kspb=0.168525;% spring constant for pulling beads

%Create Folder for Files
fn=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb'];
mkdir(fn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Read in More Parameters and Create Starting Conditions %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Read in General Parameters
txt=['Parameters.txt'];
[nav,mw,eta,diff,kt,gamma,tau,deltat,stand_dev,tot_time...
    nstep,ks,Rc,save_step,tpico,Rc_CE,ktheta,Rn_CE,ksMJ,rt] = Parameters(txt);

%Read in Contact Energies for MJ Potential
txt2=['Contact_Energies.txt'];
[CE]=load(txt2)*kt; %Multiply by kt because they are in units of kt


%Read in mass and sizes of residues
txt3=['Mass_Radius.txt'];
[MR]=load(txt3);

%Convert to SI Units
[masses]=MR(:,1)/(nav*1000);
[VDWradii]=MR(:,2)*10^-10;

%Read in residue charges (Set to 0, not used)
charge=zeros(20,1);

q=11;  % This is checked to see if we have any special connectivity.  Leave as "11" for a standard run.

%Read in AA SNARE PDB file to get residue and segment information (coordinates
%are not used)
fn=['sc15-40ns_run5_no_cpx.pdb'];
[x, y, z, bnum, res, m, a,xall,yall,zall,bnum_total,segname] = PDB_SNARE_reader(fn,masses,VDWradii);

%Read in minimized SNARE CG structure to get coordinates
fn1=['SNARE_e0_negpt25.pdb'];
[x,y,z]=txt_reader(fn1);

%Create vector from C terminus of Syx to C terminus of Syb
vSS=[x(63)-x(131),y(63)-y(131),z(63)-z(131)];
%Normalize it
magnitude=(vSS(1)^2+vSS(2)^2+vSS(3)^2)^0.5;
vSS=vSS/magnitude;

%Set ENM spring constants for all helices
ks=0;
ks = [ksnew;ksnew;ksnew;ksnew];
ksmax=max([ksMJ; ks]); %Determine maximum spring constant in the system

%Adjust parameters that are dependent on mass and size of beads
[diff,gamma,tau,deltat,stand_dev]=Parameters_mass_dependent(kt,mw,eta,a,ksmax,rt);

%Create vx, vy, vz the initial velocities of each bead
[vx,vy,vz]=Initial_Velocities(bnum_total);

%Create a matrices that contains 2 columns showing beads that are connected
%by a spring (must have at least 2 beads) PDB files corresponding to
%minimized individual helix structures
fn1=['Syb_new_2177.pdb'];
[xs1, ys1, zs1] = PDB_SNARE_reader_springs(fn1);
fn2=['Syx_new_1.pdb'];
[xs2, ys2, zs2] = PDB_SNARE_reader_springs(fn2);
fn3=['SN1_new_2199.pdb'];
[xs3, ys3, zs3] = PDB_SNARE_reader_springs(fn3);
fn4=['SN2_new_23.pdb'];
[xs4, ys4, zs4] = PDB_SNARE_reader_springs(fn4);
xs=[xs1; xs2; xs3; xs4];
ys=[ys1; ys2; ys3; ys4];
zs=[zs1; zs2; zs3; zs4];

%Create ENM Springs
[spring]=Spring_Connectivity(xs,ys,zs,Rc,bnum,ks);

%Create springs if it is unraveled up to ionic layer (but leave backbone)
spring_unraveled = zeros(1,length(spring(1,:)));
j=1;
for i=1:length(spring(:,1))
    if spring(i,2)-spring(i,1)==1
        spring_unraveled(j,:)=spring(i,:);
        j=1+j;
    elseif (spring(i,1)>29 && spring(i,1)<64) || (spring(i,2)>29 && spring(i,2)<64) 
    else
        spring_unraveled(j,:)=spring(i,:);
        j=1+j;
    end
end

%Create spring network if Syb is completely unraveled
spring_unraveled_2 = zeros(1,length(spring(1,:)));
j=1;
for i=1:length(spring(:,1))
    if spring(i,2)-spring(i,1)==1
        spring_unraveled_2(j,:)=spring(i,:);
        j=1+j;
    elseif spring(i,1)<64
    else
        spring_unraveled_2(j,:)=spring(i,:);
        j=1+j;
    end
end
    
%Create springs for FENE bonds 
fbspring=zeros(2,5);

%Syb and Syx N terminal connection
nb=15;
bl=3.65*10^-10;
delta=nb*bl;
length1 = sqrt((x(1)-x(64))^2 +(y(1)-y(64))^2 +(z(1)-z(64))^2);
fbspring(1,:)=[1 64 0.5*length1 0.2*ks(1) delta];

%SN1 and SN2 connection
nb=55;
delta=nb*bl;
length2 = sqrt((x(210)-x(276))^2 +(y(210)-y(276))^2 +(z(210)-z(276))^2);
fbspring(2,:)=[210 276 0.5*length2 0 delta];


%Creat a neighborlist for the connectivity matrix 
if length(bnum(:,1))>1 %Only do this if there is more than 1 segment
    [neighbor]=neighborlist(x,y,z,bnum,Rn_CE,CE,res,bnum_total);
end

%Create a matrix that contains 2 columns showing beads that have
%contact energies between them(must have at least 2 beads)
if length(bnum(:,1))>1
    [CE_matrix]=Contact_Energy_Connectivity(x,y,z,bnum,Rc_CE,CE,res,kt,charge,neighbor,a,lambdamult,e0mult);
else
    CE_matrix=0;
end

%Save starting coordinates for pulling beads
x63=x(63);
y63=y(63);
z63=z(63);
x131=x(131);
y131=y(131);
z131=z(131);

% Open text files to write all forces to
f1=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_x_2.txt'];
f2=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_y_2.txt'];
f3=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_z_2.txt'];
f4=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_x_syb_bead_2.txt'];
f5=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_y_syb_bead_2.txt'];
f6=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_z_syb_bead_2.txt'];
f7=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_x_syx_bead_2.txt'];
f8=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_y_syx_bead_2.txt'];
f9=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/f_z_syx_bead_2.txt'];

fidf1=fopen(f1,'a+');
fidf2=fopen(f2,'a+');
fidf3=fopen(f3,'a+');
fidf4=fopen(f4,'a+');
fidf5=fopen(f5,'a+');
fidf6=fopen(f6,'a+');
fidf7=fopen(f7,'a+');
fidf8=fopen(f8,'a+');
fidf9=fopen(f9,'a+');




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% Run Brownian Dynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Create PSF file
fn2=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/SNARE.psf'];
PSF_Writer(bnum,segname,bnum_total,fn2)

% Save initial coordinates
time=0;
X(:,1) = x;
Y(:,1) = y;
Z(:,1) = z;
fn=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/SNARE_' num2str(0) '.pdb'];
PDB_Writer(x,y,z,bnum,segname,fn)

%Adjust timestep if needed
deltat=deltat*dtmult;

%Frequency coordinates are saved to file
save_step=10000;

%Number of timesteps
nstep=20e6;

fold = 0; %0 means it's in the folded state, 1 is unfolded up to ionic layer, 2 is completely unfolded

j=1;
for istep = 2:nstep
    
    %Update neighbor list every 1000 steps
    if mod((istep),1000)==0
        if length(bnum(:,1))>1
            [neighbor]=neighborlist(x,y,z,bnum,Rn_CE,CE,res,bnum_total);
        end
    end
    
    if length(bnum(:,1))>1
        [CE_matrix]=Contact_Energy_Connectivity(x,y,z,bnum,Rc_CE,CE,res,kt,charge,neighbor,a,lambdamult,e0mult);
    end
    
    [x,y,z,fxtotal,fytotal,fztotal,fxbeadsyb,fybeadsyb,fzbeadsyb,fxbeadsyx,fybeadsyx,fzbeadsyx,fold]=Brownian_Pulling_Unraveling(x,y,z,stand_dev,...
        deltat,bnum,ks,spring,CE_matrix,ktheta,q,a,bnum_total,istep,diff,...
        kt,sigma,fbspring,x63,y63,z63,x131,y131,z131,kspb,vSS,fold,spring_unraveled,spring_unraveled_2);
    
  %Save coordinates and forces to file every save_step steps
    if mod((istep+1),save_step)==0
        
            timename=(istep*deltat*10^9);
            
            fn=['e0mult_' num2str(-e0mult) '_Rcmult_' num2str(Rcmult) '_lambdamult_' num2str(lambdamult) '_FC_Unraveling_Syb/SNARE_' num2str(j) '.pdb'];
            PDB_Writer(x,y,z,bnum,segname,fn)
         
        fprintf(fopen(f1,'a+'),'%8.3e\n',fxtotal);
        fprintf(fopen(f2,'a+'),'%8.3e\n',fytotal);
        fprintf(fopen(f3,'a+'),'%8.3e\n',fztotal);
        fprintf(fopen(f4,'a+'),'%8.3e\n',fxbeadsyb);
        fprintf(fopen(f5,'a+'),'%8.3e\n',fybeadsyb);
        fprintf(fopen(f6,'a+'),'%8.3e\n',fzbeadsyb);
        fprintf(fopen(f7,'a+'),'%8.3e\n',fxbeadsyx);
        fprintf(fopen(f8,'a+'),'%8.3e\n',fybeadsyx);
        fprintf(fopen(f9,'a+'),'%8.3e\n',fzbeadsyx);
        
        fclose(fidf1);
        fclose(fidf2);
        fclose(fidf3);
        fclose(fidf4);
        fclose(fidf5);
        fclose(fidf6);
        fclose(fidf7);
        fclose(fidf8);
        fclose(fidf9);

        j=j+1;
    end
    
end