function PSF_Writer(bnum,segname,bnum_total,fn)

fileID = fopen(fn,'w');

fprintf(fileID,'%4.0i %2s\n',bnum_total,'!NATOM');   

c=0; %counter

for seg=1:length(bnum(:,1))
    for i=c+1:c+bnum(seg,1)
        fprintf(fileID,'%8.0f %2s %4.0f %5s %3s %5s %11.6f %13.4f %11.0f\n',i,segname{seg},seg,'Syb', 'BA','BAB', 0.000000, 12.0107, 0);
    end
    c=c+bnum(seg,1);
end


fprintf(fileID,'\n');

fprintf(fileID,'%4.0f %2s\n',bnum_total-length(bnum(:,1)),'!NBOND: bonds');   

c=0;
d=0;


for seg=1:length(bnum(:,1))
    for j=c+1:c+bnum(seg,1)-1
        contact(j-d,1)=j;
        contact(j-d,2)=j+1;
    end
    d=d+1;
    c=c+bnum(seg,1);
end

k=1;
    
for i=1:length(contact(:,1))/4
    fprintf(fileID,'%8.0i %7.0f %7.0f %7.0f %7.0f %7.0f %7.0f %7.0f\n',contact(k,1),contact(k,2),contact(k+1,1),contact(k+1,2),contact(k+2,1),contact(k+2,2),contact(k+3,1),contact(k+3,2));         
    k=i*4+1;
end




fprintf(fileID,'\n');

fprintf(fileID,'%4.0f %2s\n',0,'!NTHETA: angles');   
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%4.0f %2s\n',0,'!NPHI: dihedrals');   
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%8.0f %2s\n',0,'!NDON: donors');   
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%8.0f %2s\n',0,'!NACC: acceptors');   
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%8.0f %2s\n',0,'!NNB');   
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%8.0f %8.0f %2s\n',1,0,'!NNB');   
fprintf(fileID,'%8.0f %8.0f %8.0f\n',1,0,0); 
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'\n');
fprintf(fileID,'%6.0f %2s\n',0,'!NCRTERM: cross-terms');   
fclose(fileID);

end


