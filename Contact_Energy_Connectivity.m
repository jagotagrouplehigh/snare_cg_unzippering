function [CE_matrix]=Contact_Energy_Connectivity_v6_param(x,y,z,bnum,Rc_CE,CE,res,kt,charge,neighbor,a,lambdamult,e0mult)

%#codegen
coder.inline('never')

mult=e0mult; %Multiplier for e0
mult2=lambdamult;

s=0; %Counter
%For Model A
lambda=0.159*mult2;
e0=-2.27*kt*mult;
CE=lambda.*(CE-e0);

j=0;

for i=1:length(neighbor(:,1))
    ice = neighbor(i,1);
    jce=neighbor(i,2);
    d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
    
    if d < Rc_CE %Cutoff distance
        j=j+1;
    end
end

CE_matrix=zeros(j,7);


for i=1:length(neighbor(:,1))
    ice = neighbor(i,1);
    jce=neighbor(i,2);
    d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
    
    if d < Rc_CE %Cutoff distance

        s=s+1;
        
        if abs(ice-jce)>= 3 || ice==63 ||  ice == 61 %Must have at least 3 bonds between them
            %Contact Matrix First column is the number of the atom
            %connected to the atom in column 2, column 3 is the length of
            %the spring
            
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

            
        elseif ice== 62 && jce>63
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

            
        elseif ice==61 && jce>63
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

            
        elseif ice==63
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

        elseif length(bnum) ==2
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

        else
            CE_matrix(s,1)=ice;
            CE_matrix(s,2)=jce;
            CE_matrix(s,3)=d; %Distance Between them
            if res(ice)<res(jce)
                CE_matrix(s,4)=CE(res(ice),res(jce)); %Contact Energy
            else
                CE_matrix(s,4)=CE(res(jce),res(ice)); %Contact Energy
            end
            CE_matrix(s,5)=charge(res(ice)); %charge
            CE_matrix(s,6)=charge(res(jce)); %charge

        end
        
        sigma=(2*a(ice)+2*a(jce))/2;
        CE_matrix(s,7)=sigma;


        
    end
end
end