function [neighbor]=neighborlist_v3(x,y,z,bnum,Rn_CE,CE,res,bnum_total)

%#codegen
coder.inline('never')

s=1; %Counter
c=0; %Counter
b=0;

if bnum_total<3
    for seg=1:length(bnum(:,1))
        for ice=1 % loop through each alpha carbon for helix 1
            for jce=2 % loop through each other alpha carbon for helix 1
                d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
                if d < Rn_CE %Cutoff distance
                    b=b+1;
                end
            end
        end
        c=bnum(seg,1)+c;
    end

    
else
    for seg=1:length(bnum(:,1))
        for ice=c+1:bnum(seg,1)+c % loop through each alpha carbon for helix 1
            for jce=bnum(seg,1)+c+1:bnum_total % loop through each other alpha carbon for helix 1
                d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
                
                if d < Rn_CE %Cutoff distance
                    b=b+1;
                end
            end
        end
        c=bnum(seg,1)+c;
    end
end

neighbor=zeros(b,2);

c=0;
      
if bnum_total<3
    for seg=1:length(bnum(:,1))
        for ice=1 % loop through each alpha carbon for helix 1
            for jce=2 % loop through each other alpha carbon for helix 1
                d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
                
                if d < Rn_CE %Cutoff distance
                    neighbor(s,1)=ice;
                    neighbor(s,2)=jce;
                    s=s+1;
                end
            end
            
        end
        c=bnum(seg,1)+c;
    end
    
else
    for seg=1:length(bnum(:,1))
        for ice=c+1:bnum(seg,1)+c % loop through each alpha carbon for helix 1
            for jce=bnum(seg,1)+c+1:bnum_total % loop through each other alpha carbon for helix 1
                d = ((x(ice)-x(jce))^2+(y(ice)-y(jce))^2+(z(ice)-z(jce))^2)^0.5; % Calculate distance between 2 alpha carbons
                
                if d < Rn_CE %Cutoff distance
                    neighbor(s,1)=ice;
                    neighbor(s,2)=jce;
                    s=s+1;
                end
            end
            
        end
        c=bnum(seg,1)+c;
    end
end

end