%Includes Residues for Contact energies

% Function to read a .coor file and output the x,y,z coordinates of all
% alpha carbons for syntaxin and SNAP25#1 & #2
% 1 = VAMP (Synaptobrevin)
% 2 = Syntaxin
% 3 = SNAP-25 #1
% 4 = SNAP-25 #2
% 5 = Complexin

function [xCA1, yCA1, zCA1, bnum,res,m,a,xall,yall,zall,bnum_total,segname] = PDB_SNARE_reader_v2(fn,masses,VDWradii)

fid=fopen(fn); % Open specified .coor file

C = textscan(fid, '%s %u %s %s %s %u %f %f %f %f %f %s %s','commentStyle', 'REMARK');

%Assemble Data into variables
atom=C{1};
atomno=C{2};
atomspec=C{3};
col=C{4};
letter=C{5};
residno=C{6};
x=C{7};
y=C{8};
z=C{9};
col1=C{10};
col2=C{11};
col3=C{12};
col4=C{13};



n = length(z);

%counters
p=1; %Alpha Carbons
q=1; %All Backbone atoms
t=1;


for i = 1:n
    %Find all alpha carbons
    if length(atomspec{i})==2
        
        if char(atomspec(i))=='CA'
            xCA1(p,1)=x(i,1)*10^-10;
            yCA1(p,1)=y(i,1)*10^-10;
            zCA1(p,1)=z(i,1)*10^-10;
            
            %Assign Numbers for each residue
            if char(col(i))=='CYS'
                res(p,1)=1;
                m(p,1)=masses(1);
                a(p,1)=VDWradii(1);
            elseif char(col(i))=='MET'
                res(p,1)=2;
                m(p,1)=masses(2);
                a(p,1)=VDWradii(2);
            elseif char(col(i))=='PHE'
                res(p,1)=3;
                m(p,1)=masses(3);
                a(p,1)=VDWradii(3);
            elseif char(col(i))=='ILE'
                res(p,1)=4;
                m(p,1)=masses(4);
                a(p,1)=VDWradii(4);
            elseif char(col(i))=='LEU'
                res(p,1)=5;
                m(p,1)=masses(5);
                a(p,1)=VDWradii(5);
            elseif char(col(i))=='VAL'
                res(p,1)=6;
                m(p,1)=masses(6);
                a(p,1)=VDWradii(6);
            elseif char(col(i))=='TRP'
                res(p,1)=7;
                m(p,1)=masses(7);
                a(p,1)=VDWradii(7);
            elseif char(col(i))=='TYR'
                res(p,1)=8;
                m(p,1)=masses(8);
                a(p,1)=VDWradii(8);
            elseif char(col(i))=='ALA'
                res(p,1)=9;
                m(p,1)=masses(9);
                a(p,1)=VDWradii(9);
            elseif char(col(i))=='GLY'
                res(p,1)=10;
                m(p,1)=masses(10);
                a(p,1)=VDWradii(10);
            elseif char(col(i))=='THR'
                res(p,1)=11;
                m(p,1)=masses(11);
                a(p,1)=VDWradii(11);
            elseif char(col(i))=='SER'
                res(p,1)=12;
                m(p,1)=masses(12);
                a(p,1)=VDWradii(12);
            elseif char(col(i))=='ASN'
                res(p,1)=13;
                m(p,1)=masses(13);
                a(p,1)=VDWradii(13);
            elseif char(col(i))=='GLN'
                res(p,1)=14;
                m(p,1)=masses(14);
                a(p,1)=VDWradii(14);
            elseif char(col(i))=='ASP'
                res(p,1)=15;
                m(p,1)=masses(15);
                a(p,1)=VDWradii(15);
            elseif char(col(i))=='GLU'
                res(p,1)=16;
                m(p,1)=masses(16);
                a(p,1)=VDWradii(16);
            elseif char(col(i))=='HSD'
                res(p,1)=17;
                m(p,1)=masses(17);
                a(p,1)=VDWradii(17);
            elseif char(col(i))=='ARG'
                res(p,1)=18;
                m(p,1)=masses(18);
                a(p,1)=VDWradii(18);
            elseif char(col(i))=='LYS'
                res(p,1)=19;
                m(p,1)=masses(19);
                a(p,1)=VDWradii(19);
            elseif char(col(i))=='PRO'
                res(p,1)=20;
                m(p,1)=masses(20);
                a(p,1)=VDWradii(20);
            end
            
            CAnum(p)=i; %number i where there is alpha carbon p
            
            if p>1
                if strcmp(col3{CAnum(p-1)},col3{CAnum(p)})==0
                    
                    if t==1
                        bnum(t,1)=p-1;
                        segname(t,1)=col3(CAnum(p-1));
                    else
                        bnum(t,1)=p-sum(bnum)-1;
                        segname(t,1)=col3(CAnum(p-1));
                    end
                    
                    t=t+1;
                    
                end
            end
            
            p=p+1;
        end
        
        if char(atomspec(i))=='CA' | char(atomspec(i))=='N' | char(atomspec(i))=='C'
            xall(q,1)=x(i,1)*10^-10;
            yall(q,1)=y(i,1)*10^-10;
            zall(q,1)=z(i,1)*10^-10;
            q=q+1;
        end
    end
    
    
end

    bnum(t,1)=p-sum(bnum)-1; %Enter the last bead number
    segname(t,1)=col3(CAnum(p-1));
    
%     for seg=length(bnum(:,1))
%         segname(seg)=['P' num2str(seg) 'ns.pdb'];
%     segname=[{'P1'}];%;{'P2'};{'P3'};{'P4'}];
    bnum_total=sum(bnum); %Number of beads

fclose('all');
end


