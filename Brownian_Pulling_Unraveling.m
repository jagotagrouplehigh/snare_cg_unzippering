function [x_new,y_new,z_new,fxtotal,fytotal,fztotal,fxbeadsyb,fybeadsyb,...
    fzbeadsyb,fxbeadsyx,fybeadsyx,fzbeadsyx,fold]=Brownian_Pulling_Unraveling(x,y,z,...
    stand_dev,deltat,bnum,ks,spring,CE_matrix,ktheta,q,a,bnum_total,istep,diff,kt,sigma,fbspring,x63,y63,z63,x131,y131,z131,kspb,vSS,fold,spring_unraveled,spring_unraveled_2)

%Calculate Random Forces
[frx,fry,frz] = Force_Random(bnum_total,stand_dev);

%Calculate MJ Forces
turn_off_MJ = 0;
if length(bnum(:,1))>1
    [fcex,fcey,fcez] = Force_Contact_Energy(bnum_total,x,y,z,CE_matrix,a,istep);
    if turn_off_MJ == 1
      fcex=0;
      fcey=0;
      fcez=0;
    end
else
    fcex=0;
    fcey=0;
    fcez=0;
end

%Calculate FENE bond forces
[ffbx, ffby, ffbz]=Force_FENE_Bond(x,y,z,fbspring,bnum_total);

%Calculate force on pulling beads
[fpbx, fpby, fpbz]=Force_pulling_beads(x,y,z,bnum_total,x63,y63,z63,x131,y131,z131,kspb);
fpbx(63,1)=((2.125e-18)*(istep-1))*vSS(1);
fpby(63,1)=((2.125e-18)*(istep-1))*vSS(2);
fpbz(63,1)=((2.125e-18)*(istep-1))*vSS(3);

%Folding/Unfolding Algorithm
dtswap = 10; %Check raveling/unraveling every 10 steps
deltaE = 37.4201;
lambdas = 2.1193;
F = (10^12)*sqrt(fpbx(63,1)^2+fpby(63,1)^2+fpbz(63,1)^2);
expfac = exp(-deltaE + F*lambdas);
Pu = expfac/(1+expfac); %Probability of Unfolding
Pf = 1-Pu; %Probability of Folding

%Reimplement ENM according to unraveling/raveling
if fold==2 %If completely unfolded, leave it that way
    [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring_unraveled_2,x,y,z);
elseif fold==0 %In folded state, will it unfold?
    if rand<Pu && mod((istep-1),dtswap)==0
        fold=1; %unfolds
        [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring_unraveled,x,y,z);
    else
        fold=0;
        [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring,x,y,z);
    end
elseif fold==1 %In unfolded state, will it fold?
    if rand<Pf && mod((istep-1),dtswap)==0
        fold=0; %folds
        [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring,x,y,z);
    elseif rand<Pu && mod((istep-1),dtswap)==0
        fold=2; %unfolds completely
        [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring_unraveled_2,x,y,z);
    else
        fold=1; %stays unfolded up to ionic layer
        [fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring_unraveled,x,y,z);
    end
end

%Calculate new coordinates using Brownian dynamics
x_new = x+(frx+fspx+fcex+ffbx+fpbx).*diff*(deltat/kt);
y_new = y+(fry+fspy+fcey+ffby+fpby).*diff*(deltat/kt);
z_new = z+(frz+fspz+fcez+ffbz+fpbz).*diff*(deltat/kt);

%Save Forces
fxtotal=[(fspx(63)+fcex(63)+ffbx(63));(fspx(131)+fcex(131)+ffbx(131))];
fytotal=[(fspy(63)+fcey(63)+ffby(63));(fspy(131)+fcey(131)+ffby(131))];
fztotal=[(fspz(63)+fcez(63)+ffbz(63));(fspz(131)+fcez(131)+ffbz(131))];
fxbeadsyb=fpbx(63);
fybeadsyb=fpby(63);
fzbeadsyb=fpbz(63);
fxbeadsyx=fpbx(131);
fybeadsyx=fpby(131);
fzbeadsyx=fpbz(131);



end

