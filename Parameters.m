%Has parameters for contact energies

function [nav,mw,eta,diff,kt,gamma,tau,deltat,stand_dev,tot_time...
    nstep,ks,Rc,save_step,tpico,Rc_CE,ktheta,Rn_CE,ksMJ,rt] = Parameters_v6(txt)

% Read in all parameter values from excel sheet
[num]=load(txt);

% Assign them to their correct variable names
mw = num(1);
eta = num(2);
tot_time = num(3);
ks = [num(4);num(5);num(6);num(7);num(8)];
Rc = num(9);
a = num(10);
Rc_CE = num(11);
ktheta = num(12);
ksMJ = num(13);
rt = num(14);

nav = 6.02E+23;
kt = 4.14E-21; %At room temperature
gamma = 6*pi*eta*a/mw;
diff = kt/(mw*gamma);
tau = (max([ks',ksMJ])/mw)^-0.5;
deltat = tau/20;
stand_dev = kt*(2/(diff*deltat))^0.5;
nstep = tot_time/deltat;
tpico = round(tot_time/(10*10^-12))+1;
save_step = round(round(nstep/tpico)/10);
Rn_CE = Rc_CE*1.5;




end