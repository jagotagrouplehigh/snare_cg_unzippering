function [fpbx, fpby, fpbz]=Force_pulling_beads(x,y,z,bnum_total,x63,y63,z63,x131,y131,z131,kspb)

%#codegen
coder.inline('never')

fpbx = zeros(bnum_total,1);
fpby = zeros(bnum_total,1);
fpbz = zeros(bnum_total,1);

%Spring on Syb
l_syb=sqrt((x(63)-x63)^2+(y(63)-y63)^2+(z(63)-z63)^2);
spring=0; %length of spring
ks=kspb;

if l_syb==0
    fpbxtemp = 0;
    fpbytemp = 0;
    fpbztemp = 0;
else
    fpbxtemp = -ks*(x(63)-x63);%(1-spring/l_syb)
    fpbytemp = -ks*(y(63)-y63);%(1-spring/l_syb)
    fpbztemp = -ks*(z(63)-z63);%(1-spring/l_syb)
end

fpbx(63,1) = fpbx(63,1) + fpbxtemp;
fpby(63,1) = fpby(63,1) + fpbytemp;
fpbz(63,1) = fpbz(63,1) + fpbztemp;

%Spring on Syx
l_syx=sqrt((x(131)-x131)^2+(y(131)-y131)^2+(z(131)-z131)^2);
spring=0; %length of spring
ks=kspb;

if l_syx==0
    fpbxtemp = 0;
    fpbytemp = 0;
    fpbztemp = 0;
else
    fpbxtemp = -ks*(x(131)-x131);%(1-spring/l_syb)
    fpbytemp = -ks*(y(131)-y131);%(1-spring/l_syb)
    fpbztemp = -ks*(z(131)-z131);%(1-spring/l_syb)
end

fpbx(131,1) = fpbx(131,1) + fpbxtemp;
fpby(131,1) = fpby(131,1) + fpbytemp;
fpbz(131,1) = fpbz(131,1) + fpbztemp;


end


