function [spring]=Spring_Connectivity(x,y,z,Rc,bnum,ks)

s=1; %Counter

c=0; %counter

for seg=1:length(bnum(:,1))
    ks_seg=ks(seg);
    
    for ispr=c+1:bnum(seg,1)+c-1 % loop through each alpha carbon for helix 1
        for jspr=ispr+1:c+bnum(seg,1) % loop through each other alpha carbon for helix 1
            d = ((x(ispr)-x(jspr))^2+(y(ispr)-y(jspr))^2+(z(ispr)-z(jspr))^2)^0.5; % Calculate distance between 2 alpha carbons
            
            if d < Rc
                %Contact Matrix First column is the number of the atom
                %connected to the atom in column 2, column 3 is the length of
                %the spring
                spring(s,1)=ispr;
                spring(s,2)=jspr;
                spring(s,3)=d;
                spring(s,4)=ks_seg;
                s=s+1; %counter
                
            end
        end
    end
    c=bnum(seg,1)+c;
end

end