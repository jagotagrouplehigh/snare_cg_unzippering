# SNARE_CG_Unzippering
This is a coarse-grained Brownian dynamics simulation code for SNARE including 
Synaptobrevin, Syntaxin, and SNAP25 based on PDB structure 1N7S. The individual
helix structures are maintained using an elastic network model. These springs have
been calibrated by matching the fluctuations of all atom simulations of individual 
SNARE helices. The helix-helix interactions are represented using Miyazawa and 
Jernigan forces that are amino acid specific. The parameters have been calibrated
to match experimental results as well as the all atom  SNARE structure. The purpose of 
this code is to disassemble a SNARE bundle using force control. The simulation 
begins with a minimized SNARE bundle structure (one has been provided or the 
user can use their own). A fixed bead is attached to the C-terminal end of Syx, and
a pulling bead is attached to the C-terminal end of Syb (to which force control is
applied). This model also includes force dependent melting/helix reformation of Syb 
that is applied using a Monte Carlo algorithm.

Files Provided:
1. Contact_Energies.txt: Includes parameters for MJ forces
2. Mass_Radius.txt: Includes mass/radius values for all amino acids
3. Parameters.txt: Includes parameters such as viscosity, temperature, etc.
4. sc15-40ns_run5_no_cpx.pdb: A full all atom PDB file of SNARE. This is used for 
   chemical information such as amino acids types, NOT coordinates
5. SN1_new_2199.pdb, SN2_new_23.pdb, Syb_new_2177.pdb, Syx_new_1.pdb: These are
   minimized structures for individual helices used to create elastic network springs
6. SNARE_e0_negpt25.pdb: This is the CG starting structure. A minimized SNARE bundle.

To Run This Code:

1. Have all provided files in the same folder.
2. Run MAIN_CODE.m in MATLAB
3. A results folder will automatically be created which includes:
	1. A PSF file that can be used in VMD
	2. Multiple SNARE PDB files that are printed every "save_step" timesteps
	3. Force files containing the TOTAL forces on the C-terminal Syb/Syx beads
	   printed every "save_step" timesteps
	4. Force files containing the dummy bead forces on the C-terminal Syb/Syx beads
	   printed every "save_step" timesteps
