function [diff,gamma,tau,deltat,stand_dev]=Parameters_mass_dependent(kt,m,eta,a,ks,rt)

tau=1/sqrt(ks/min(m(:,1))); %Based on the smallest mass

deltat=tau/20;

gamma=6*pi*eta*(a./m);

diff=(kt./(m.*gamma));%*(rt/300);

stand_dev=(kt*sqrt(2./(deltat.*diff)));%*(rt/300);

end