function[fspx,fspy,fspz,l] = Force_Spring(bnum_total,spring,x,y,z)

%#codegen
coder.inline('never')

fspx = zeros(bnum_total,1);
fspy = zeros(bnum_total,1);
fspz = zeros(bnum_total,1);


% Determine number of springs (i.e. number of rows in spring array)

c = length(spring(:,1));
l = zeros(c,1);

for i = 1:c
    
    %Calculate distance between 2 beads connected by a spring
    l(i,1) = sqrt((x(spring(i,1))-x(spring(i,2)))^2+(y(spring(i,1))-y(spring(i,2)))^2+(z(spring(i,1))-z(spring(i,2)))^2);
    
    ks=spring(i,4);
    
    %Calculate force due to spring (F = -kx)
    
    fspxtemp = -ks*(x(spring(i,1))-x(spring(i,2)))*(1-spring(i,3)/l(i,1));
    fspytemp = -ks*(y(spring(i,1))-y(spring(i,2)))*(1-spring(i,3)/l(i,1));
    fspztemp = -ks*(z(spring(i,1))-z(spring(i,2)))*(1-spring(i,3)/l(i,1));
    
    %Calculate forces on each bead in opposite directions. Add this to the
    %existing forces due to other springs on the given beads
    fspx(spring(i,1),1) = fspx(spring(i,1),1) + fspxtemp;
    fspy(spring(i,1),1) = fspy(spring(i,1),1) + fspytemp;
    fspz(spring(i,1),1) = fspz(spring(i,1),1) + fspztemp;
    
    fspx(spring(i,2),1) = fspx(spring(i,2),1) - fspxtemp;
    fspy(spring(i,2),1) = fspy(spring(i,2),1) - fspytemp;
    fspz(spring(i,2),1) = fspz(spring(i,2),1) - fspztemp;
        
end

end

