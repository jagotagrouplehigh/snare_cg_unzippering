%Includes Residues for Contact energies

% Function to read a .coor file and output the x,y,z coordinates of all
% alpha carbons for syntaxin and SNAP25#1 & #2
% 1 = VAMP (Synaptobrevin)
% 2 = Syntaxin
% 3 = SNAP-25 #1
% 4 = SNAP-25 #2
% 5 = Complexin

function [xCA1, yCA1, zCA1] = PDB_SNARE_reader_springs(fn)

%counters
p=1; %Alpha Carbons

fid=fopen(fn);

C = textscan(fid, '%s %u %s %s %u %f %f %f %f %f','commentStyle', 'REMARK');

%Assemble Data into variables
atom=C{1};
atomno=C{2};
atomspec=C{3};
res=C{4};
residno=C{5};
x=C{6};
y=C{7};
z=C{8};
col1=C{9};
col2=C{10};

fclose('all');

n = length(z);


for i = 1:n
    %Find all alpha carbons
    if length(atomspec{i})==2
        if char(atomspec(i))=='CA'
            xCA1(p,1)=x(i,1)*10^-10;
            yCA1(p,1)=y(i,1)*10^-10;
            zCA1(p,1)=z(i,1)*10^-10;
            p=p+1;
        end
        
        
    end
    
end


fclose('all');
end


