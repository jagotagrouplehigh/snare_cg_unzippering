function [fcex,fcey,fcez] = Force_Contact_Energy(bnum,x,y,z,CE_matrix,a,istep)

%#codegen
coder.inline('never')

fcex = zeros(bnum,1);
fcey = zeros(bnum,1);
fcez = zeros(bnum,1);

% Determine number of residue-residue conacts (i.e. number of rows in CE array)

c = length(CE_matrix(:,1));
eij = CE_matrix(:,4); 

% %Multiplier to scale sigma
if istep<=101
    mult=(0.4+0.6*istep/101);
else
    mult=1;
end

sigma=zeros(c);

CE_force=0; %Define it for MEX

sigma = CE_matrix(:,3); %Distance between the atoms at the start

%Create connectivity matrix
for i = 1:c
       
    %Bead Numbers    b1=CE_matrix(i,1);
    b1=CE_matrix(i,1);
    b2=CE_matrix(i,2);
    
    sigma(i)=mult*0.4*CE_matrix(i,7);
    
    %Calculate distance between 2 residues in contact
    r = sqrt((x(b1)-x(b2))^2+(y(b1)-y(b2))^2+(z(b1)-z(b2))^2);
    rij0 = (2^(1/6))*sigma(i);
    
    rx = x(b1)-x(b2);
    ry = y(b1)-y(b2);
    rz = z(b1)-z(b2);
      
    if eij(i)<0
        %Calculate force due to CE_matrix (F = -kx)
        
        CE_force = ((24*abs(eij(i)))/r^2)*((2*(sigma(i)/r)^12)-((sigma(i)/r)^6));
        
    elseif eij(i)>0
        if r<rij0
            CE_force = ((24*eij(i))/r^2)*((2*(sigma(i)/r)^12)-((sigma(i)/r)^6));
        elseif r>=rij0
            CE_force = -((24*eij(i))/r^2)*((2*(sigma(i)/r)^12)-((sigma(i)/r)^6));
        end
    else %if eij is 0
        CE_force=0;
    end
    
    %Calculate forces on each bead in opposite directions. Add this to the
    %existing forces due to other springs on the given beads
    fcex(CE_matrix(i,1),1) = fcex(CE_matrix(i,1),1) + CE_force*rx;
    fcey(CE_matrix(i,1),1) = fcey(CE_matrix(i,1),1) + CE_force*ry;
    fcez(CE_matrix(i,1),1) = fcez(CE_matrix(i,1),1) + CE_force*rz;
    
    fcex(CE_matrix(i,2),1) = fcex(CE_matrix(i,2),1) - CE_force*rx;
    fcey(CE_matrix(i,2),1) = fcey(CE_matrix(i,2),1) - CE_force*ry;
    fcez(CE_matrix(i,2),1) = fcez(CE_matrix(i,2),1) - CE_force*rz;
    
end


end

