function PDB_Writer(x,y,z,bnum,segname,fn)

x=x*10^10;
y=y*10^10;
z=z*10^10;

%load Syb_Assembly_Coord.mat

fileID = fopen(fn,'w');

c=0; %counter


for j = 1:length(bnum(:,1))
    for i=1+c:bnum(j,1)+c
        fprintf(fileID,'%4s %6u %3s %4s %1s %3u %11.3f %7.3f %7.3f %5.2f %5.2f %3s\n','ATOM',i, 'BA', 'Syb', 'P', i,x(i),y(i),z(i), 1.00, 0.00,  'B');
    end
    c=bnum(j,1)+c;
end

fclose(fileID);

end


